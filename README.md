Pasos para iniciar proyecto.
1.- Ejecutar npm install para instalar las dependencias
2.- Cambiar en src/config.js la URI de conexion de mongoDB (Actualmente esta la URI con la conexion de Atlas)
3.- npm run dev para ejecutar en entorno dev

- controllers
  Guarda los controladores
- libs
  Contiene archivos de inicializacion (Seeders)
- middlewares
  Contiene intermediarios para la validacion de datos contenidos en peticiones y de autenticacion
- models
  Modelos de datos
- routes
  Contiene las rutas con sus respectivos metodos HTTP
- schema
  Contiene la estructura a validar peticiones de acuerdo a cada modelo
