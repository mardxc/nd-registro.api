import { signIn } from '../src/controllers/auth.controller.js';

describe('Auth controller', () => {
    jest.setTimeout(30000)
    describe('Sign In', () => {
        const res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
            send: jest.fn()
        }
        const req = {
            body: {
                email: 'amedinaf95@gmail.com',
                password: '123tamarindo'
            }
        }
        it('Login returns json string', async () => {
            await signIn(req, res)
            expect(res.json.mock.calls).toEqual([[{ message: expect.any(String) }]])
        });
        it('Login returns status code', async () => {
            expect(res.status.mock.calls).toEqual([[500]])
        });
    });
});