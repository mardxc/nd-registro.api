import request from 'supertest'
import express from 'express'
import authRoutes from '../src/routes/auth.routes'
import mongoose from "mongoose";
import config from "../src/config"
import User from '../src/models/User'

const app = express()
app.use('/api/auth', authRoutes)

const user = {
    email: 'amedinaf95@gmail.com',
    password: '123tamarindo',
    name: 'Mardxc'
}
beforeEach(async () => {
    await User.deleteMany()
    const user1 = new User(user)
    await user1.save()
})

beforeAll((done) => {
    // Mongoose object conection options
    const connectionOptions = {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
    }

    mongoose
        .connect(config.MONGODB_URI, connectionOptions, () => done())
});

afterAll((done) => {
    mongoose.connection.close(() => done());
});

describe('POST /users', () => {
    jest.setTimeout(30000)
    it('responds with json', async (done) => {
        const response = await request(app)
            .post('/api/auth/signin')
            .send({
                email: user.email,
                password: user.password
            })
            .set('Accept', 'application/json')
        expect(response.status).toBe(500)
    })
})