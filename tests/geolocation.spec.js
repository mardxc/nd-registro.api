import { calculateGeolocation, isValidLocation } from "../src/libs/geolocation"

describe('Geolocation libs', () => {
    jest.setTimeout(30000)
    it('Validar ubicacion', async () => {
        const pUser = [
            20.00,
            -100.00
        ]
        const p1 = { lat: 20.01, lon: -100.1 }
        const p2 = { lat: 20.01, lon: -100.1 }
        const valid = await isValidLocation(pUser, p1, p2)
        expect(valid).toBe(false);

    });
    it('Calcular ubicacion', async () => {
        const location = await calculateGeolocation(20.00, -100.01)
        expect(location).toEqual({
            p1: '20.000016000000002,-100.00991999206401',
            p2: '19.999984000012798,-100.010080008'
        });
    });
});