import { Schema, model } from 'mongoose'

const Role = Schema({
    name: String,
}, { 
    versionKey: false
})

export default model('Role', Role)
