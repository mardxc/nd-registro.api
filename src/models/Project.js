import { Schema, model } from "mongoose";

const Project = Schema(
  {
    code: String,
    name: String,
    type: {
      type: Schema.Types.ObjectId,
      ref: "Type",
    },
    lifecycle: {
      type: Schema.Types.ObjectId,
      ref: "Lifecycle",
    },
    estimateModel: {
      type: Schema.Types.ObjectId,
      ref: "EstimationModel",
    },
    start: Date,
    end: Date,
    duration: Number,
    coverage: String,
    salesHours: {
      type: Schema.Types.Decimal128,
    },
    estimateHours: {
      type: Schema.Types.Decimal128,
    },
    cosmicSize: {
      type: Schema.Types.Decimal128,
    },
    productivity: {
      type: Schema.Types.Decimal128,
    },
    percentDefects: {
      type: Schema.Types.Decimal128,
    },
    percentDefectsClient: {
      type: Schema.Types.Decimal128,
    },
    salesAmount: {
      type: Schema.Types.Decimal128,
    },
    staffCost: {
      type: Schema.Types.Decimal128,
    },
    projectCost: {
      type: Schema.Types.Decimal128,
    },
    projectBudget: {
      type: Schema.Types.Decimal128,
    },
    profit: {
      type: Schema.Types.Decimal128,
    },
    percentProfit: {
      type: Schema.Types.Decimal128,
    },
    salesRate: {
      type: Schema.Types.Decimal128,
    },
    client: String,
    responsable: String,
    projectManager: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    leader: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    collaborators: [
      {
        type: Schema.Types.ObjectId,
        ref: "User",
      },
    ],
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Project", Project);
