import { Schema, model } from "mongoose";

const EstimationModel = Schema(
  {
    description: { type: String, unique: true },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("EstimationModel", EstimationModel);
