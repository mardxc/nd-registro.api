import { Schema, model } from "mongoose";

const Task = Schema(
  {
    start: Date,
    end: Date,
    activity: String,
    description: String,
    hours: { type: Number },
    hours_worked: { type: Number },
    task_noplanned: { type: Boolean, default: false },
    delay_desc: String,
    criteria: [{ description: String, criteria: String }],
    status: {
      type: Schema.Types.ObjectId,
      ref: "Status",
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    project: {
      type: Schema.Types.ObjectId,
      ref: "Project",
    },
    phase: {
      type: Schema.Types.ObjectId,
      ref: "Phase",
    },
    delay: {
      type: Schema.Types.ObjectId,
      ref: "Delay",
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Task", Task);
