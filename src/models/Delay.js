import { Schema, model } from "mongoose";

const Delay = Schema(
  {
    description: { type: String, unique: true },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Delay", Delay);
