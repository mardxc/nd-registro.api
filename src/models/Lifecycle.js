import { Schema, model } from "mongoose";

const Lifecycle = Schema(
  {
    description: { type: String, unique: true },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Lifecycle", Lifecycle);
