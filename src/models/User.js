import { Schema, model } from 'mongoose'
import bcrypt from 'bcryptjs'

const User = Schema({

    username: { type: String},
    email: { type: String, unique: true, },
    password: { type: String, required: true, },
    name: String,
    lastname: String,
    birthday: Date,
    roles: [{
        type: Schema.Types.ObjectId,
        ref: 'Role'
    }],
    location: [{
        type: Schema.Types.ObjectId,
        ref: 'Geolocation'
    }]
}, {
    timestamps: true,
    versionKey: false
})

User.statics.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10)
    return await bcrypt.hash(password, salt)
}
User.statics.comparePassword = async (password, recivePassword) => {
    return await bcrypt.compare(password, recivePassword)
}


export default model('User', User)
