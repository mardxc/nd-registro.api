import { Schema, model } from 'mongoose'

const Activity = Schema({
    description: { type: String, unique: true, },
}, {
    timestamps: true,
    versionKey: false
})

export default model('Activity', Activity)
