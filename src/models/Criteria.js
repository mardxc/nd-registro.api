import { Schema, model } from "mongoose";

const Criteria = Schema(
  {
    description: { type: String, unique: true },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Criteria", Criteria);
