import { Schema, model } from 'mongoose'

const Geolocation = Schema({
    descripcion: String,
    p1: String,
    p2: String,
    activo: { type: Number, default: 1 }

}, { 
    timestamps: true,
    versionKey: false
})

export default model('Geolocation', Geolocation)
