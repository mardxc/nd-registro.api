import { Schema, model } from 'mongoose'

const Status = Schema({
    description: { type: String, unique: true, },
}, {
    timestamps: true,
    versionKey: false
})

export default model('Status', Status)
