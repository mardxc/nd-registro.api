import { Schema, model } from "mongoose";

const Type = Schema(
  {
    description: { type: String, unique: true },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Type", Type);
