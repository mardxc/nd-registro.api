import { Schema, model } from 'mongoose'

const Workday = Schema({
    start: Date,
    activity: String,
    location: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    activity: {
        type: Schema.Types.ObjectId,
        ref: 'Activity'
    }
}, {
    versionKey: false
})

export default model('Workday', Workday)
