import { Schema, model } from 'mongoose'

const Phase = Schema({
    description: { type: String, unique: true, },
}, {
    timestamps: true,
    versionKey: false
})

export default model('Phase', Phase)
