import jwt from "jsonwebtoken"
import config from "../config"
import User from "../models/User"
import Role from "../models/Role"
import Geolocation from "../models/Geolocation"

export const createUser = async (req, res) => {
    try {
        const { username, email, password, roles, name, lastname, birthday, locations } = req.body

        const rolesFound = await Role.find({ name: { $in: roles } })
        const locationsFound = await Geolocation.find({ name: { $in: locations } })

        const user = new User({
            username,
            email,
            password,
            name,
            lastname,
            birthday,
            roles: rolesFound.map((role) => role._id),
            location: locationsFound.map((loc) => loc._id)
        })

        user.password = await User.encryptPassword(user.password)

        const savedUser = await user.save()

        res.status(201).json({
            message: "User created",
            data: [{
                _id: savedUser._id,
                username: savedUser.username,
                email: savedUser.email,
                roles: savedUser.roles,
            }]
        })
    } catch (error) {
        
        return res.status(500).json({ message: error.message })
    }
}

export const getUsers = async (req, res) => {
    try {
        const user = await User.find({}, { password: 0 }).populate('roles')
        res.status(200).json({
            message: 'Fetch Users',
            data: user
        })

    } catch (error) {
        
        return res.status(500).json({ message: error.message })
    }
}

export const getUser = async (req, res) => {
    try {
        const { id } = req.params
        const user = await User.findById(id, { password: 0 })
        res.status(200).json({
            message: 'Fetch User',
            data: user
        })

    } catch (error) {
        
        return res.status(500).json({ message: error.message })
    }
}

export const deleteUser = async (req, res) => {
    try {
        const { id } = req.params

        const user = await User.remove({ _id: id })
        res.status(200).json({ message: 'User deleted' })

    } catch (error) {
        
        return res.status(500).json({ message: error.message })
    }
}
export const changePassword = async (req, res) => {
    let token = req.headers["x-access-token"]
    if (!token) return res.status(403).json({ message: "No token provided" })
    const { password } = req.body
    try {

        const decoded = jwt.verify(token, config.SECRET)
        const userId = decoded.id

        const user = await User
            .updateOne(
                { _id: userId },
                { $set: { "password": await User.encryptPassword(password) } },
            )

        if (user.nModified == 1) {
            res.status(200).json({ message: 'Password updated' })
        } else {
            res.status(400).json({ message: 'Password no updated' })
        }

    } catch (error) {
        
        return res.status(500).json({ message: error.message })
    }
}

export const updateUser = async (req, res) => {
    let token = req.headers["x-access-token"]
    if (!token) return res.status(403).json({ message: "No token provided" })
    const { username } = req.body
    try {

        const decoded = jwt.verify(token, config.SECRET)
        const userId = decoded.id
        const user = await User
            .updateOne(
                { _id: userId },
                { $set: { username: username } },
            )

        if (user.nModified == 1) {
            res.status(200).json({ message: 'User updated' })
        } else {
            res.status(500).json({ message: 'User no updated' })
        }

    } catch (error) {
        
        return res.status(500).json({ message: error.message })
    }
}
export const updateUserById = async (req, res) => {
    const { id } = req.params
    const { username, lastname, name, email, birthday } = req.body
    try {

        const user = await User
            .updateOne(
                { _id: id },
                {
                    $set: {
                        username: username,
                        name: name,
                        lastname: lastname,
                        email: email,
                        birthday: birthday
                    }
                },
            )

        if (user.nModified == 1) {
            res.status(200).json({ message: 'User updated' })
        } else {
            res.status(500).json({ req: req.body, message: 'User no updated' })
        }

    } catch (error) {
        
        return res.status(500).json({ message: error.message })
    }
}

export const getMe = async (req, res) => {
    let token = req.headers["x-access-token"]

    if (!token) return res.status(403).json({ message: "No token provided" })

    try {
        const decoded = jwt.verify(token, config.SECRET)
        req.userId = decoded.id

        const user = await User.findById(req.userId, { password: 0 }).populate('roles')
        res.status(200).json({ data: user })
    } catch (error) {
        
        return res.status(401).json({ message: "Unauthorized!" })
    }
}