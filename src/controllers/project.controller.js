import Project from "../models/Project";
import User from "../models/User";

export const getProjects = async (req, res) => {
  try {
    const project = await Project.find()
      .populate("collaborators", ["name", "lastname", "email"])
      .populate("leader", ["name", "lastname", "email"])
      .populate("projectManager", ["name", "lastname", "email"])
      .populate("projectManager", ["name", "lastname", "email"]);
    res.status(200).json({
      message: "Fetch Projects",
      data: project,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createProject = async (req, res) => {
  try {
    const {
      code,
      name,
      type,
      lifecycle,
      start,
      end,
      duration,
      coverage,
      salesHours,
      estimateHours,
      estimateModel,
      cosmicSize,
      productivity,
      percentDefects,
      percentDefectsClient,
      salesAmount,
      staffCost,
      projectCost,
      projectBudget,
      profit,
      percentProfit,
      salesRate,
      client,
      responsable,
      projectManager,
      leader,
      collaborators,
    } = req.body;
    const collaboratorsFound = await User.find({ _id: { $in: collaborators } });

    const project = new Project({
      code,
      name,
      type,
      lifecycle,
      start,
      end,
      duration,
      coverage,
      salesHours,
      estimateHours,
      estimateModel,
      cosmicSize,
      productivity,
      percentDefects,
      percentDefectsClient,
      salesAmount,
      staffCost,
      projectCost,
      projectBudget,
      profit,
      percentProfit,
      salesRate,
      client,
      responsable,
      projectManager,
      leader,
      collaborators: collaboratorsFound.map((user) => user._id),
    });

    const savedProject = await project.save();

    res.status(201).json({
      message: "Project created",
      data: savedProject,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateProject = async (req, res) => {
  const { id } = req.params;
  const {
    code,
    name,
    type,
    lifecycle,
    start,
    end,
    duration,
    coverage,
    salesHours,
    estimateHours,
    estimateModel,
    cosmicSize,
    productivity,
    percentDefects,
    percentDefectsClient,
    salesAmount,
    staffCost,
    projectCost,
    projectBudget,
    profit,
    percentProfit,
    salesRate,
    client,
    responsable,
    projectManager,
    leader,
    collaborators,
  } = req.body;

  try {
    const collaboratorsFound = await User.find({ _id: { $in: collaborators } });

    const obj = await Project.updateOne(
      { _id: id },
      {
        $set: {
          code,
          name,
          type,
          lifecycle,
          start,
          end,
          duration,
          coverage,
          salesHours,
          estimateHours,
          estimateModel,
          cosmicSize,
          productivity,
          percentDefects,
          percentDefectsClient,
          salesAmount,
          staffCost,
          projectCost,
          projectBudget,
          profit,
          percentProfit,
          salesRate,
          client,
          responsable,
          projectManager,
          leader,
          collaborators: collaboratorsFound.map((user) => user._id),
        },
      }
    );

    if (obj.nModified == 1) {
      res.status(200).json({ message: "Project updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Project no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteProject = async (req, res) => {
  try {
    const { id } = req.params;
    await Project.remove({ _id: id });
    res.status(200).json({ message: "Project deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditProject = async (req, res) => {
  try {
    const { id } = req.params;
    const obj = await Project.findById(id).populate("collaborators", ["name"]);
    res.status(200).json({
      message: "Fetch Project",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
