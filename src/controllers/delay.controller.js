import Delay from "../models/Delay";

export const getDelayies = async (req, res) => {
  try {
    const delay = await Delay.find();

    res.status(200).json({
      message: "Fetch Delayies",
      data: delay,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createDelay = async (req, res) => {
  try {
    const { description } = req.body;

    const delay = new Delay({
      description,
    });

    const savedDelay = await delay.save();

    res.status(201).json({
      message: "Delay created",
      data: savedDelay,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteDelay = async (req, res) => {
  try {
    const { id } = req.params;
    await Delay.remove({ _id: id });
    res.status(200).json({ message: "Delay deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditDelay = async (req, res) => {
  try {
    const { id } = req.params;
    const obj = await Delay.findById(id);
    res.status(200).json({
      message: "Fetch Delay",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateDelay = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const obj = await Delay.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (obj.nModified == 1) {
      res.status(200).json({ message: "Delay updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Delay no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
