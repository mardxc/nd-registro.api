import User from "../models/User";
import jwt from "jsonwebtoken";
import config from "../config";
import Role from "../models/Role";
import Geolocation from "../models/Geolocation";

export const signUp = async (req, res) => {
  try {
    const {
      username,
      email,
      password,
      roles,
      name,
      lastname,
      birthday,
      locations,
    } = req.body;

    const newUser = new User({
      username,
      email: email.toLowerCase(),
      password,
      name,
      lastname,
      birthday,
      password: await User.encryptPassword(password),
    });
    if (roles) {
      const findRole = await Role.find({ name: { $in: roles } });
      newUser.roles = findRole.map((role) => role._id);
    } else {
      const role = await Role.findOne({ name: "colaborator" });
      newUser.roles = [role._id];
    }
    if (locations) {
      const findLocation = await Geolocation.find({ name: { $in: locations } });
      newUser.roles = findLocation.map((location) => location._id);
    }
    const savedUser = await newUser.save();

    const token = jwt.sign({ id: savedUser._id }, config.SECRET, {
      //expiresIn: 86400
    });

    return res.status(201).json({ token, message: "Register success" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
export const signIn = async (req, res) => {
  try {
    const { email, password } = req.body;

    const userFound = await User.findOne({ email: email }).populate("roles");

    if (!userFound) return res.status(400).json({ message: "User not found" });

    const matchPassword = await User.comparePassword(
      password,
      userFound.password
    );

    if (!matchPassword)
      return res.status(401).json({ message: "Invalid Password" });

    const token = jwt.sign({ id: userFound._id }, config.SECRET, {
      //expiresIn: 86400, // 24 hours
    });

    res.status(200).json({ token, message: "Login success" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
