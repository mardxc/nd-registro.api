import Task from "../models/Task";
import Project from "../models/Project";
import Phase from "../models/Phase";
import Status from "../models/Status";
import User from "../models/User";
import { parseJson } from "../libs/xlsx.parse";

export const getTasks = async (req, res) => {
  try {
    const task = await Task.find().populate("user", [
      "name",
      "lastname",
      "email",
    ]);
    //.populate('status', ['description'])
    //.populate('phase', ['description'])
    //.populate('project', ['description'])

    res.status(200).json({
      message: "Fetch Tasks",
      data: task,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getTasksByUser = async (req, res) => {
  try {
    const { id } = req.params;

    const task = await Task.find({ user: id })
      .sort({ _id: -1 })
      .populate("user", ["name", "lastname", "email"]);
    //.populate('status', ['description'])
    //.populate('phase', ['description'])
    //.populate('project', ['description'])

    res.status(200).json({
      message: "Fetch Tasks",
      data: task,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getTasksByUserDay = async (req, res) => {
  try {
    const { userId, date } = req.body;
    const task = [];
    const tasks = await Task.find({
      user: userId,
    })
      .sort({ _id: -1 })
      .populate("user", ["name", "lastname", "email"])
      .populate("status", ["description"])
      .populate("phase", ["description"])
      .populate("project", ["name"]);
    for (const item in tasks) {
      const day = tasks[item].start.getDate();
      const month = tasks[item].start.getMonth() + 1;
      const year = tasks[item].start.getFullYear();
      const today = `${year}-${month}-${day}`;
      const compDate = new Date(date);
      const compToday = new Date(today);
      if (compDate.getTime() === compToday.getTime()) {
        task.push(tasks[item]);
      }
    }

    res.status(200).json({
      message: "Fetch Tasks",
      data: task,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getTasksByUserDate = async (req, res) => {
  try {
    const { id } = req.params;
    const { start, end } = req.body;

    const task = await Task.find({
      user: id,
      start: {
        $gte: new Date(new Date(start).setHours("00", "00", "00")),
        $lt: new Date(new Date(end).setHours(23, 59, 59)),
      },
    })
      .sort({ _id: -1 })
      .populate("user", ["name", "lastname", "email"])
      .populate("status", ["description"])
      .populate("phase", ["description"])
      .populate("project", ["name"]);

    res.status(200).json({
      message: "Fetch Tasks",
      data: task,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getTasksById = async (req, res) => {
  try {
    const { id } = req.params;

    const task = await Task.findById(id)
      .sort({ _id: -1 })
      .populate("user", ["name", "lastname", "email"]);
    //.populate('status', ['description'])
    //.populate('phase', ['description'])
    //.populate('project', ['description'])

    res.status(200).json({
      message: "Fetch Task",
      data: task,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createTask = async (req, res) => {
  try {
    const {
      start,
      end,
      description,
      userId,
      status,
      hours,
      activity,
      projectId,
      phase,
    } = req.body;

    const task = new Task({
      start: new Date(start),
      end: new Date(end),
      activity,
      description,
      hours,
      project: projectId,
      phase: phase,
      status: status,
      user: userId,
    });

    const savedTask = await task.save();

    res.status(201).json({
      message: "Task created",
      data: savedTask,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateTask = async (req, res) => {
  const {
    start,
    end,
    description,
    status,
    hours,
    activity,
    projectId,
    phase,
    taskId,
    hours_worked,
    delayId,
    task_noplanned,
    delay_desc,
    criteria,
  } = req.body;
  try {
    const criterios = [];
    for (const key in criteria) {
      const element = criteria[key];
      criterios.push({
        description: element,
        criteria: key,
      });
    }
    const task = await Task.updateOne(
      { _id: taskId },
      {
        $set: {
          activity: activity,
          start: new Date(start),
          end: new Date(end),
          description: description,
          status: status,
          hours: hours,
          project: projectId,
          phase: phase,
          hours_worked: hours_worked,
          delay: delayId,
          task_noplanned: task_noplanned,
          delay_desc: delay_desc,
          criteria: criterios,
        },
      }
    );

    if (task.nModified == 1) {
      res.status(200).json({ message: "Task updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Task no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateStatusTask = async (req, res) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const task = await Task.updateOne(
      { _id: id },
      {
        $set: {
          status: status,
        },
      }
    );

    if (task.nModified == 1) {
      res.status(200).json({ message: "Status updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Status no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getTasksByProjectDate = async (req, res) => {
  try {
    const { id } = req.params;
    const { start, end } = req.body;

    const task = await Task.find({
      project: id,
      start: {
        $gte: new Date(new Date(start).setHours("00", "00", "00")),
        $lt: new Date(new Date(end).setHours(23, 59, 59)),
      },
    })
      .sort({ _id: -1 })
      .populate("user", ["name", "lastname", "email"])
      .populate("status", ["description"])
      .populate("phase", ["description"])
      .populate("project", ["name"]);

    res.status(200).json({
      message: "Fetch Tasks",
      data: task,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getTasksFromFile = async (req, res) => {
  try {
    const task = parseJson();

    res.status(200).json({
      message: "Fetch Tasks",
      data: task,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createTasksFromFile = async (req, res) => {
  try {
    const taskJson = parseJson();
    const moment = require("moment");

    const importTask = [];
    for (let i = 0; i < taskJson.length; i++) {
      const dato = taskJson[i];
      const project = await Project.findOne({
        name: dato.project,
      });
      const phase = await Phase.findOne({
        description: dato.phase,
      });
      const status = await Status.findOne({
        description: dato.status,
      });
      const user = await User.findOne({
        username: dato.user,
      });
      var parts = dato.start.split("/");
      var start = `${parts[2]}-${parts[1]}-${parts[0]}`;

      parts = dato.end.split("/");
      var end = `${parts[2]}-${parts[1]}-${parts[0]}`;

      const task = new Task({
        start: new Date(start),
        end: new Date(end),
        activity: dato.activity,
        description: dato.description,
        hours: dato.hours,
        project: project._id,
        phase: phase._id,
        status: status._id,
        user: user?._id,
      });
      console.log(task);
      const savedTask = await task.save();
      importTask.push(savedTask);
    }

    res.status(200).json({
      message: "Import Tasks",
      data: importTask,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
