import Lifecycle from "../models/Lifecycle";

export const getLifecycle = async (req, res) => {
  try {
    const obj = await Lifecycle.find();

    res.status(200).json({
      message: "Fetch Lifecycle",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditLifecycle = async (req, res) => {
  try {
    const { id } = req.params;
    const obj = await Lifecycle.findById(id);
    res.status(200).json({
      message: "Fetch Lifecycle",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateLifecycle = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const obj = await Lifecycle.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (obj.nModified == 1) {
      res.status(200).json({ message: "Lifecycle updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Lifecycle no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createLifecycle = async (req, res) => {
  try {
    const { description } = req.body;

    const obj = new Lifecycle({
      description,
    });

    const saved = await obj.save();

    res.status(201).json({
      message: "Lifecycle created",
      data: saved,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteLifecycle = async (req, res) => {
  try {
    const { id } = req.params;
    await Lifecycle.remove({ _id: id });
    res.status(200).json({ message: "Lifecycle deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
