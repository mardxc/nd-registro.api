import Type from "../models/Type";

export const getType = async (req, res) => {
  try {
    const obj = await Type.find();

    res.status(200).json({
      message: "Fetch Type",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditType = async (req, res) => {
  try {
    const { id } = req.params;
    const obj = await Type.findById(id);
    res.status(200).json({
      message: "Fetch Type",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateType = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const obj = await Type.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (obj.nModified == 1) {
      res.status(200).json({ message: "Type updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Type no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createType = async (req, res) => {
  try {
    const { description } = req.body;

    const obj = new Type({
      description,
    });

    const saved = await obj.save();

    res.status(201).json({
      message: "Type created",
      data: saved,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteType = async (req, res) => {
  try {
    const { id } = req.params;
    await Type.remove({ _id: id });
    res.status(200).json({ message: "Type deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
