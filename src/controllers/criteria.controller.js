import Criteria from "../models/Criteria";

export const getCriteria = async (req, res) => {
  try {
    const obj = await Criteria.find();

    res.status(200).json({
      message: "Fetch Criteria",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createCriteria = async (req, res) => {
  try {
    const { description } = req.body;

    const obj = new Criteria({
      description,
    });

    const savedCriteria = await obj.save();

    res.status(201).json({
      message: "Criteria created",
      data: savedCriteria,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteCriteria = async (req, res) => {
  try {
    const { id } = req.params;
    await Criteria.remove({ _id: id });
    res.status(200).json({ message: "Criteria deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditCriteria = async (req, res) => {
  try {
    const { id } = req.params;
    const obj = await Criteria.findById(id);
    res.status(200).json({
      message: "Fetch Criteria",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateCriteria = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const obj = await Criteria.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (obj.nModified == 1) {
      res.status(200).json({ message: "Criteria updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Criteria no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
