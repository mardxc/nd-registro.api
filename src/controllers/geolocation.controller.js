import { calculateGeolocation } from "../libs/geolocation";
import Geolocation from '../models/Geolocation'
import User from "../models/User"

export const createGeolocation = async (req, res) => {
    try {
        const { name, lat, lon } = req.body

        const getCoordinates = await calculateGeolocation(lat, lon)

        const location = new Geolocation({
            descripcion: name,
            p1: getCoordinates.p1,
            p2: getCoordinates.p2
        })
        const savedLocation = await location.save()

        res.status(201).json({
            message: "Location created",
            data: savedLocation
        })
    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}

export const getLocations = async (req, res) => {
    try {
        const location = await Geolocation.find()
        res.status(200).json({
            message: `Fetch ${location.length} registers`,
            data: location
        })

    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}

export const getLocationsByUser = async (req, res) => {
    try {
        const { id } = req.params
        const user = await User.findById(id, { location: 1 })

        const findLocation = await Geolocation.find({ _id: { $in: user.location } })

        res.status(200).json({
            message: 'Fetch Locations',
            data: {
                location: findLocation,
                userId: id
            },
        })

    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}

export const unassingLocationUser = async (req, res) => {
    try {
        const { userId, locations } = req.body
        const user = await User.updateMany({ _id: userId },
            { $pull: { location: { _id: locations } } })

        if (user.nModified == 1) {
            res.status(200).json({ message: 'Location unassined' })
        } else {
            res.status(500).json({ message: 'Location not unassined' })
        }
    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}
export const assingLocationUser = async (req, res) => {
    try {
        const { userId, locations } = req.body
        const userLoc = await User.findById(userId, { location: 1 })
        userLoc.location.push(locations)
        userLoc.save()

        res.status(200).json({ message: 'Location assined' })

    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}