import Phase from "../models/Phase";

export const getPhases = async (req, res) => {
  try {
    const phase = await Phase.find();

    res.status(200).json({
      message: "Fetch Phases",
      data: phase,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createPhase = async (req, res) => {
  try {
    const { description } = req.body;

    const phase = new Phase({
      description,
    });

    const savedPhase = await phase.save();

    res.status(201).json({
      message: "Phase created",
      data: savedPhase,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deletePhase = async (req, res) => {
  try {
    const { id } = req.params;
    await Phase.remove({ _id: id });
    res.status(200).json({ message: "Phase deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditPhase = async (req, res) => {
  try {
    const { id } = req.params;
    const phase = await Phase.findById(id);
    res.status(200).json({
      message: "Fetch Phase",
      data: phase,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updatePhase = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const phase = await Phase.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (phase.nModified == 1) {
      res.status(200).json({ message: "Phase updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Phase no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
