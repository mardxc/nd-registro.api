import EstimationModel from "../models/EstimationModel";

export const getEstimationModel = async (req, res) => {
  try {
    const obj = await EstimationModel.find();

    res.status(200).json({
      message: "Fetch Estimation Model",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditEstimationModel = async (req, res) => {
  try {
    const { id } = req.params;
    const obj = await EstimationModel.findById(id);
    res.status(200).json({
      message: "Fetch Estimation Model",
      data: obj,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateEstimationModel = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const obj = await EstimationModel.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (obj.nModified == 1) {
      res.status(200).json({ message: "Estimation Model updated" });
    } else {
      res
        .status(500)
        .json({ req: req.body, message: "Estimation Model no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createEstimationModel = async (req, res) => {
  try {
    const { description } = req.body;

    const obj = new EstimationModel({
      description,
    });

    const saved = await obj.save();

    res.status(201).json({
      message: "Estimation Model created",
      data: saved,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteEstimationModel = async (req, res) => {
  try {
    const { id } = req.params;
    await EstimationModel.remove({ _id: id });
    res.status(200).json({ message: "Estimation Model deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
