import Activity from "../models/Activity";

export const getActivities = async (req, res) => {
  try {
    const activity = await Activity.find();

    res.status(200).json({
      message: "Fetch Activities",
      data: activity,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createActivity = async (req, res) => {
  try {
    const { description } = req.body;

    const activity = new Activity({
      description,
    });

    const savedActivity = await activity.save();

    res.status(201).json({
      message: "Activity created",
      data: savedActivity,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteActivity = async (req, res) => {
  try {
    const { id } = req.params;
    await Activity.remove({ _id: id });
    res.status(200).json({ message: "Activity deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditActivity = async (req, res) => {
  try {
    const { id } = req.params;
    const activity = await Activity.findById(id);
    res.status(200).json({
      message: "Fetch Activity",
      data: activity,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateActivity = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const activity = await Activity.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (activity.nModified == 1) {
      res.status(200).json({ message: "Activity updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Activity no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
