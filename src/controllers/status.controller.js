import Status from "../models/Status";

export const getStatus = async (req, res) => {
  try {
    const status = await Status.find();

    res.status(200).json({
      message: "Fetch Status",
      data: status,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const getEditStatus = async (req, res) => {
  try {
    const { id } = req.params;
    const status = await Status.findById(id);
    res.status(200).json({
      message: "Fetch Status",
      data: status,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const updateStatus = async (req, res) => {
  const { id } = req.params;
  const { description } = req.body;
  try {
    const status = await Status.updateOne(
      { _id: id },
      {
        $set: {
          description: description,
        },
      }
    );

    if (status.nModified == 1) {
      res.status(200).json({ message: "Status updated" });
    } else {
      res.status(500).json({ req: req.body, message: "Status no updated" });
    }
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const createStatus = async (req, res) => {
  try {
    const { description } = req.body;

    const status = new Status({
      description,
    });

    const savedStatus = await status.save();

    res.status(201).json({
      message: "Status created",
      data: savedStatus,
    });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

export const deleteStatus = async (req, res) => {
  try {
    const { id } = req.params;
    await Status.remove({ _id: id });
    res.status(200).json({ message: "Status deleted" });
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};
