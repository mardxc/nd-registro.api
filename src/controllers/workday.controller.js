import jwt from "jsonwebtoken"
import config from "../config"
import Workday from "../models/Workday"
import User from "../models/User"
import { isValidLocation } from "../libs/geolocation";


export const checkIn = async (req, res) => {
    try {
        const { lat, lon, activity } = req.body

        let token = req.headers["x-access-token"]
        if (!token) return res.status(403).json({ message: "No token provided" })
        const decoded = jwt.verify(token, config.SECRET)
        const userId = decoded.id

        const user = await User.find({ _id: userId }, { username: 1, email: 1 }).populate('location')
        const location = user.map((loc) => loc.location[0])

        if (location != '') {

            const geolocation = []
            location.forEach(element => {
                geolocation.push(
                    {
                        p1: element.p1,
                        p2: element.p2
                    }
                )
            });
            const p1 = geolocation[0].p1.split(',')
            const p2 = geolocation[0].p2.split(',')
            const pUser = [
                lat, lon
            ];
            const validate = await isValidLocation(pUser, p1, p2)

            if (validate) {

                const workday = new Workday({
                    start: Date.now(),
                    location: `${lat}, ${lon}`,
                    activity,
                    user: userId
                })
                await workday.save()

                res.status(201).json({
                    message: "Activity Registred",
                    data: workday
                })

            } else {
                res.status(400).json({ message: 'Location no valid' })
            }
        } else {
            res.status(400).json({
                message: "User has not location assign"
            })
        }
    } catch (error) {

        return res.status(500).json({ message: error.message })
    }


}

export const getChecks = async (req, res) => {
    try {
        let token = req.headers["x-access-token"]
        if (!token) return res.status(403).json({ message: "No token provided" })
        const decoded = jwt.verify(token, config.SECRET)
        const userId = decoded.id

        const workday = await Workday.find({ user: userId })
        .populate('activity', ['description'])
        .sort({ _id: -1 })

        res.status(200).json({
            data: workday
        })

    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}
export const getChecksById = async (req, res) => {
    try {
        const { userId } = req.params

        const workday = await Workday.find({ user: userId })
        .populate('activity', ['description'])
        .sort({ _id: -1 })

        res.status(200).json({
            data: workday,
            userId
        })

    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}

export const getChecksByIdDate = async (req, res) => {
    try {
        const { userId, date } = req.body
        const workday = await Workday.find({
            user: userId,
            start: {
                $gte: new Date(new Date(date).setHours('00', '00', '00')),
                $lt: new Date(new Date(date).setHours(23, 59, 59))
            },
        })
        .populate('activity', ['description'])
        .sort({ _id: -1 })

        res.status(200).json({
            data: workday,
            userId
        })

    } catch (error) {

        return res.status(500).json({ message: error.message })
    }
}