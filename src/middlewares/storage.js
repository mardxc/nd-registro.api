import multer from "multer";

const path = require("path");

export const storage = multer.diskStorage({
  destination: "./storage",
  filename: (req, file, cb) => {
    const name = `plannig${path.extname(file.originalname)}`;
    cb(null, name);
  },
});

export const uploadFile = multer({
  storage,
  fileFilter: (req, file, cb) => {
    const fileTypes = /sheet/;
    const fileExt = /xlsx/;
    const mimeType = fileTypes.test(file.mimetype);
    const extName = fileExt.test(path.extname(file.originalname));
    if (mimeType && extName) {
      return cb(null, true);
    } else {
      cb("Only .xlsx format allowed!");
    }
  },
}).single("planning_file");
