import express, { json } from "express";
import morgan from "morgan";
import cors from "cors";

import {
  createRoles,
  createAdmin,
  createDefaulLocation,
  createActivity,
  createDelay,
  createLifecycle,
  createPhase,
  createStatus,
  createType,
  createEstimationModel,
  createCriteria,
} from "./libs/initialSetup";

import authRoutes from "./routes/auth.routes";
import usersRoutes from "./routes/user.routes";
import geolocationRoutes from "./routes/geolocation.routes";
import workdayRoutes from "./routes/workday.routes";
import taskRoutes from "./routes/task.routes";
import phaseRoutes from "./routes/phase.routes";
import projectRoutes from "./routes/project.routes";
import statusRoutes from "./routes/status.routes";
import activityRoutes from "./routes/activity.routes";
import delayRoutes from "./routes/delay.routes";
import lifecycleRoutes from "./routes/lifecycle.routes";
import typeRoutes from "./routes/type.routes";
import estimationRoutes from "./routes/estimation_model.routes";
import criteriaRoutes from "./routes/criteria.routes";

const app = express();
createRoles();
createAdmin();
createDefaulLocation();
createActivity();
createDelay();
createLifecycle();
createPhase();
createStatus();
createType();
createEstimationModel();
createCriteria();

app.set("port", process.env.PORT || 3000);

app.set("json spaces", 2);

app.use(morgan("dev"));
app.use(express.json());
app.use(cors());

app.use("/api/auth", authRoutes);
app.use("/api/users", usersRoutes);
app.use("/api/location", geolocationRoutes);
app.use("/api/workday", workdayRoutes);
app.use("/api/task", taskRoutes);
app.use("/api/phase", phaseRoutes);
app.use("/api/project", projectRoutes);
app.use("/api/status", statusRoutes);
app.use("/api/activity", activityRoutes);
app.use("/api/delay", delayRoutes);
app.use("/api/lifecycle", lifecycleRoutes);
app.use("/api/type", typeRoutes);
app.use("/api/estimation_model", estimationRoutes);
app.use("/api/criteria", criteriaRoutes);

app.get("*", (req, res) => {
  res.status(404).send("Error");
});

export default app;
