import XLSX from "xlsx";

export const parseJson = () => {
  const excel = XLSX.readFile("storage/plannig.xlsx");
  var nombreHoja = excel.SheetNames; // regresa un array
  let datos = XLSX.utils.sheet_to_json(excel.Sheets[nombreHoja[0]], {
    raw: false,
  });

  const jDatos = [];
  for (let i = 0; i < datos.length; i++) {
    const dato = datos[i];

    if (dato.__EMPTY_1 != "Tarea") {
      jDatos.push({
        project: dato.__EMPTY,
        activity: dato.__EMPTY_1,
        description: dato.__EMPTY_2,
        phase: dato.__EMPTY_3,
        start: dato.__EMPTY_4,
        end: dato.__EMPTY_5,
        hours: dato.__EMPTY_6,
        hours_worked: dato.__EMPTY_7,
        status: dato.__EMPTY_8,
        user: dato.__EMPTY_9,
      });
    }
  }
  return jDatos;
};
