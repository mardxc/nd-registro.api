import Role from "../models/Role";
import Activity from "../models/Activity";
import Delay from "../models/Delay";
import Lifecycle from "../models/Lifecycle";
import Phase from "../models/Phase";
import Status from "../models/Status";
import Type from "../models/Type";
import EstimationModel from "../models/EstimationModel";
import User from "../models/User";
import Criteria from "../models/Criteria";
import Geolocation from "../models/Geolocation";

import bcrypt from "bcryptjs";

import { calculateGeolocation } from "../libs/geolocation";

export const createRoles = async () => {
  try {
    const count = await Role.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Role({ name: "colaborator" }).save(),
      new Role({ name: "admin" }).save(),
    ]);

    console.log("Create Role");
  } catch (error) {}
};

export const createDefaulLocation = async () => {
  const location = await Geolocation.find();
  if (location.length < 1) {
    const getCoordinates = await calculateGeolocation(20.609635, -100.431638);

    await Geolocation.create({
      descripcion: "Default",
      p1: getCoordinates.p1,
      p2: getCoordinates.p2,
    });

    console.log("Default Location Created!");
  }
};

export const createAdmin = async () => {
  const user = await User.findOne({ email: "amedinaf95@gmail.com" });

  const roles = await Role.find({ name: { $in: ["admin"] } });
  const locations = await Geolocation.find({
    descripcion: { $in: ["Default"] },
  });

  if (!user) {
    await User.create({
      username: "admin",
      email: "amedinaf95@gmail.com",
      password: await bcrypt.hash("admin", 10),
      roles: roles.map((role) => role._id),
      location: locations.map((location) => location._id),
    });
    console.log("Admin User Created!");
  }
};

export const createActivity = async () => {
  try {
    const count = await Activity.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Activity({ description: "Inicio jornada" }).save(),
      new Activity({ description: "Inicio comida" }).save(),
      new Activity({ description: "Fin comida" }).save(),
      new Activity({ description: "Fin jornada" }).save(),
    ]);

    console.log("Create Activity");
  } catch (error) {}
};

export const createDelay = async () => {
  try {
    const count = await Delay.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Delay({ description: "Cambio de requerimientos" }).save(),
      new Delay({ description: "Falta de insumos del cliente" }).save(),
    ]);

    console.log("Create Delay");
  } catch (error) {}
};

export const createLifecycle = async () => {
  try {
    const count = await Lifecycle.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Lifecycle({ description: "Cascada" }).save(),
      new Lifecycle({ description: "Iterativo Incremental" }).save(),
      new Lifecycle({ description: "Agil" }).save(),
    ]);

    console.log("Create Lifecycle");
  } catch (error) {}
};

export const createPhase = async () => {
  try {
    const count = await Phase.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Phase({ description: "Modelado" }).save(),
      new Phase({ description: "Desarrollo" }).save(),
      new Phase({ description: "Pruebas" }).save(),
      new Phase({ description: "Definicion de requerimientos" }).save(),
    ]);

    console.log("Create Phase");
  } catch (error) {}
};

export const createStatus = async () => {
  try {
    const count = await Status.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Status({ description: "Pendiente" }).save(),
      new Status({ description: "Planeada" }).save(),
      new Status({ description: "En proceso" }).save(),
      new Status({ description: "Finalizada" }).save(),
    ]);

    console.log("Create Status");
  } catch (error) {}
};

export const createType = async () => {
  try {
    const count = await Type.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Type({ description: "Soporte" }).save(),
      new Type({ description: "Desarrollo" }).save(),
    ]);

    console.log("Create Type");
  } catch (error) {}
};

export const createEstimationModel = async () => {
  try {
    const count = await EstimationModel.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new EstimationModel({ description: "Juicio de expertos" }).save(),
      new EstimationModel({ description: "Puntos COSMIC" }).save(),
      new EstimationModel({ description: "Puntos de Funcion" }).save(),
    ]);

    console.log("Create EstimationModel");
  } catch (error) {}
};

export const createCriteria = async () => {
  try {
    const count = await Criteria.estimatedDocumentCount();

    if (count > 0) return;

    const values = await Promise.all([
      new Criteria({ description: "Pruebas" }).save(),
      new Criteria({ description: "Despliegue en ambiente" }).save(),
    ]);

    console.log("Create Criteria");
  } catch (error) {}
};
