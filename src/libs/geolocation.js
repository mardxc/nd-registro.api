
export const calculateGeolocation = async (lat, lon) => {
    try {
        const multiplier = 1.0000008
        /// Al multiplicar por 1.000018 se obtiene aprox un agregado de 8.6m, por lo que latitud tendera a subir en Y
        const upLat = lat * multiplier
        /// Longitud permanece sin cambios para poder marcar el punto maximo en y, el punto resultante es up(x,2y)
        const upLon = lon

        /// Al divide entre 1.000018 se obtiene aprox una disminucion de 8.6m, por lo que latitud tendera a bajar en y
        const downLat = lat / multiplier
        /// Longitud permanece sin cambios para poder marcar el punto minimo en y, el punto resultante es down(x,-2y)
        const downLon = lon

        /// Latitud permanece sin cambios para poder marcar el punto minimo en x
        const leftLat = lat
        /// Al multiplicar por 1.000018 se obtiene aprox una disminucion de 8.6m, por lo que latitud tendera a la izquierda en x
        ///  el punto resultante es down(-2x,y)
        const leftLon = lon * multiplier

        /// Latitud permanece sin cambios para poder marcar el punto maximo en x
        const rightLat = lat
        /// Al dividir entre 1.000018 se obtiene aprox un agregado de 8.6m, por lo que latitud tendera a la izquierda en x
        ///  el punto resultante es down(2x,y)
        const rightLon = lon / multiplier

        /// Obtenidos los 4 puntos en eje x y eje y, se obtiene el punto de la esquina superior derecha
        const upRightLat = upLat
        const upRightLon = rightLon
        const p1 = `${upRightLat},${upRightLon}`

        /// Obtenidos los 4 puntos en eje x y eje y, se obtiene el punto de la esquina inferior izquierda
        const downLeftLat = downLat
        const downLeftLon = leftLon
        const p2 = `${downLeftLat},${downLeftLon}`

        const location = {
            p1,
            p2
        }
        return location
    } catch (error) {

    }
}

export const isValidLocation = async (pUser, p1, p2) => {
    const latUser = parseFloat(pUser[0])
    const lonUser = parseFloat(pUser[1])

    const latP1 = parseFloat(p1[0])
    const lonP1 = parseFloat(p1[1])

    const latP2 = parseFloat(p2[0])
    const lonP2 = parseFloat(p2[1])
    var isValidLat = false
    var isValidLon = false
    var isValid = false;

    if (latUser >= latP2 && latUser <= latP1) {
        isValidLat = true
    }
    if (lonUser <= lonP1 && lonUser >= lonP2) {
        isValidLon = true
    }
    if (isValidLat && isValidLon) {
        isValid = true
    }
    return isValid

}