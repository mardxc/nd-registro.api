import { Router } from 'express'

import * as workdayCtrl from '../controllers/workday.controller'
import { authJwt, validator } from "../middlewares"
import { workdaySchema } from "../schema"

const router = Router()

router.post(
    "/checkin",
    [
        authJwt.verifyToken,
        /*workdaySchema.checkIn,
        validator.validateRequestSchema,*/
    ],
    workdayCtrl.checkIn
)
router.get(
    "/",
    [
        authJwt.verifyToken,
    ],
    workdayCtrl.getChecks
)
router.post(
    "/checks",
    [
        authJwt.verifyToken,
        /*workdaySchema.getChecksByIdDate,
        validator.validateRequestSchema,*/
    ],
    workdayCtrl.getChecksByIdDate
)
router.get(
    "/:userId",
    [
        authJwt.verifyToken,
    ],
    workdayCtrl.getChecksById
)

export default router
