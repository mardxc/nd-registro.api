import { Router } from "express";

import * as projectCtrl from "../controllers/project.controller";
import { authJwt, validator } from "../middlewares";

const router = Router();

router.post("/", [authJwt.verifyToken], projectCtrl.createProject);

router.get("/", [authJwt.verifyToken], projectCtrl.getProjects);
router.get("/:id", [authJwt.verifyToken], projectCtrl.getEditProject);
router.put("/:id", [authJwt.verifyToken], projectCtrl.updateProject);

router.delete("/:id", [authJwt.verifyToken], projectCtrl.deleteProject);

export default router;
