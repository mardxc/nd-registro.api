import { Router } from "express";

import * as LifecycleCtrl from "../controllers/lifecycle.controller";
import { authJwt } from "../middlewares";

const router = Router();

router.post("/", [authJwt.verifyToken], LifecycleCtrl.createLifecycle);

router.get("/", [authJwt.verifyToken], LifecycleCtrl.getLifecycle);
router.get("/:id", [authJwt.verifyToken], LifecycleCtrl.getEditLifecycle);
router.put("/:id", [authJwt.verifyToken], LifecycleCtrl.updateLifecycle);

router.delete("/:id", [authJwt.verifyToken], LifecycleCtrl.deleteLifecycle);

export default router;
