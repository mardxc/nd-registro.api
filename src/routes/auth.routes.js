import { Router } from 'express'

import * as authController from '../controllers/auth.controller'
import { validator } from "../middlewares"
import { authSchema } from "../schema"

const router = Router()

router.post(
    "/signin",
    [
        authSchema.loginSchema,
        validator.validateRequestSchema,
    ],
    authController.signIn
)

router.post(
    "/signup",
    [
        authSchema.registerSchema,
        validator.validateRequestSchema,
    ],
    authController.signUp
)

export default router
