import { Router } from "express"
const router = Router()

import * as geolocationCtrl from "../controllers/geolocation.controller"
import { authJwt, validator } from "../middlewares"
import { geolocationSchema } from "../schema"

router.post(
    "/",
    [
        authJwt.verifyToken,
        authJwt.isAdmin,
        geolocationSchema.createGeolocation,
        validator.validateRequestSchema,
    ],
    geolocationCtrl.createGeolocation
)
router.get(
    "/",
    [
        authJwt.verifyToken,
        validator.validateRequestSchema,
    ],
    geolocationCtrl.getLocations
)
router.get(
    "/:id",
    [
        authJwt.verifyToken,
        validator.validateRequestSchema,
    ],
    geolocationCtrl.getLocationsByUser
)
router.post(
    "/assing-location",
    [
        authJwt.verifyToken,
        validator.validateRequestSchema,
        geolocationSchema.assingLocationUser,
        validator.validateRequestSchema,
    ],
    geolocationCtrl.assingLocationUser
)
router.post(
    "/unassing-location",
    [
        authJwt.verifyToken,
        /*geolocationSchema.assingLocationUser,
        validator.validateRequestSchema,*/
    ],
    geolocationCtrl.unassingLocationUser
)
export default router