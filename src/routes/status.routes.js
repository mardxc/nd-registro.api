import { Router } from "express";

import * as statusCtrl from "../controllers/status.controller";
import { authJwt, validator } from "../middlewares";
import { statusSchema } from "../schema";

const router = Router();

router.post(
  "/",
  [authJwt.verifyToken, statusSchema.create, validator.validateRequestSchema],
  statusCtrl.createStatus
);

router.get("/", [authJwt.verifyToken], statusCtrl.getStatus);
router.get("/:id", [authJwt.verifyToken], statusCtrl.getEditStatus);
router.put("/:id", [authJwt.verifyToken], statusCtrl.updateStatus);

router.delete("/:id", [authJwt.verifyToken], statusCtrl.deleteStatus);

export default router;
