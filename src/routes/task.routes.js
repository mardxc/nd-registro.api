import { Router } from "express";

import * as taskCtrl from "../controllers/task.controller";
import { authJwt, validator, storage } from "../middlewares";
import { taskSchema } from "../schema";

const router = Router();

router.post(
  "/",
  [authJwt.verifyToken, taskSchema.createTask, validator.validateRequestSchema],
  taskCtrl.createTask
);

router.get("/", [authJwt.verifyToken], taskCtrl.getTasks);

router.get("/:id", [authJwt.verifyToken], taskCtrl.getTasksById);

router.get("/user/:id", [authJwt.verifyToken], taskCtrl.getTasksByUser);

router.post("/parse", [authJwt.verifyToken], taskCtrl.getTasksFromFile);
router.post(
  "/upload-task",
  [authJwt.verifyToken, storage.uploadFile],
  taskCtrl.createTasksFromFile
);

router.post("/user/:id", [authJwt.verifyToken], taskCtrl.getTasksByUserDate);
router.post("/user", [authJwt.verifyToken], taskCtrl.getTasksByUserDay);

router.post("/update", [authJwt.verifyToken], taskCtrl.updateTask);

router.put("/status/:id", [authJwt.verifyToken], taskCtrl.updateStatusTask);

router.post(
  "/project/:id",
  [authJwt.verifyToken],
  taskCtrl.getTasksByProjectDate
);

export default router;
