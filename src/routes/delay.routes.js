import { Router } from "express";

import * as delayCtrl from "../controllers/delay.controller";
import { authJwt, validator } from "../middlewares";
import { delaySchema } from "../schema";

const router = Router();

router.post(
  "/",
  [authJwt.verifyToken, delaySchema.create, validator.validateRequestSchema],
  delayCtrl.createDelay
);

router.get("/", [authJwt.verifyToken], delayCtrl.getDelayies);
router.get("/:id", [authJwt.verifyToken], delayCtrl.getEditDelay);
router.put("/:id", [authJwt.verifyToken], delayCtrl.updateDelay);

router.delete("/:id", [authJwt.verifyToken], delayCtrl.deleteDelay);

export default router;
