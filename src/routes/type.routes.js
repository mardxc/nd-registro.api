import { Router } from "express";

import * as typecleCtrl from "../controllers/type.controller";
import { authJwt } from "../middlewares";

const router = Router();

router.post("/", [authJwt.verifyToken], typecleCtrl.createType);

router.get("/", [authJwt.verifyToken], typecleCtrl.getType);
router.get("/:id", [authJwt.verifyToken], typecleCtrl.getEditType);
router.put("/:id", [authJwt.verifyToken], typecleCtrl.updateType);

router.delete("/:id", [authJwt.verifyToken], typecleCtrl.deleteType);

export default router;
