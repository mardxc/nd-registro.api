import { Router } from "express";

import * as phaseCtrl from "../controllers/phase.controller";
import { authJwt, validator } from "../middlewares";
import { phaseSchema } from "../schema";

const router = Router();

router.post(
  "/",
  [authJwt.verifyToken, phaseSchema.create, validator.validateRequestSchema],
  phaseCtrl.createPhase
);

router.get("/", [authJwt.verifyToken], phaseCtrl.getPhases);
router.get("/:id", [authJwt.verifyToken], phaseCtrl.getEditPhase);
router.put("/:id", [authJwt.verifyToken], phaseCtrl.updatePhase);

router.delete("/:id", [authJwt.verifyToken], phaseCtrl.deletePhase);

export default router;
