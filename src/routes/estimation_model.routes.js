import { Router } from "express";

import * as estimationCtrl from "../controllers/estimation_model.controller";
import { authJwt } from "../middlewares";

const router = Router();

router.post("/", [authJwt.verifyToken], estimationCtrl.createEstimationModel);

router.get("/", [authJwt.verifyToken], estimationCtrl.getEstimationModel);
router.get(
  "/:id",
  [authJwt.verifyToken],
  estimationCtrl.getEditEstimationModel
);
router.put("/:id", [authJwt.verifyToken], estimationCtrl.updateEstimationModel);

router.delete(
  "/:id",
  [authJwt.verifyToken],
  estimationCtrl.deleteEstimationModel
);

export default router;
