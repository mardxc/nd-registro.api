import { Router } from "express";

import * as activityCtrl from "../controllers/activity.controller";
import { authJwt, validator } from "../middlewares";
import { activitySchema } from "../schema";

const router = Router();

router.post(
  "/",
  [authJwt.verifyToken, activitySchema.create, validator.validateRequestSchema],
  activityCtrl.createActivity
);

router.get("/", [authJwt.verifyToken], activityCtrl.getActivities);
router.get("/:id", [authJwt.verifyToken], activityCtrl.getEditActivity);
router.put("/:id", [authJwt.verifyToken], activityCtrl.updateActivity);

router.delete("/:id", [authJwt.verifyToken], activityCtrl.deleteActivity);

export default router;
