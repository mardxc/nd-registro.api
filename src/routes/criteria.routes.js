import { Router } from "express";

import * as criteriaCtrl from "../controllers/criteria.controller";
import { authJwt, validator } from "../middlewares";
import { delaySchema } from "../schema";

const router = Router();

router.post(
  "/",
  [authJwt.verifyToken, delaySchema.create, validator.validateRequestSchema],
  criteriaCtrl.createCriteria
);

router.get("/", [authJwt.verifyToken], criteriaCtrl.getCriteria);
router.get("/:id", [authJwt.verifyToken], criteriaCtrl.getEditCriteria);
router.put("/:id", [authJwt.verifyToken], criteriaCtrl.updateCriteria);

router.delete("/:id", [authJwt.verifyToken], criteriaCtrl.deleteCriteria);

export default router;
