import { Router } from "express"
const router = Router()

import * as usersCtrl from "../controllers/user.controller"
import { authJwt, verifySignup, validator } from "../middlewares"
import { userSchema } from "../schema"

router.post(
    "/",
    [
        authJwt.verifyToken,
        authJwt.isAdmin,
        verifySignup.checkDuplicateUsernameOrEmail,
        //userSchema.createUser,
        //validator.validateRequestSchema,
    ],
    usersCtrl.createUser
)
router.get(
    "/",
    [
        authJwt.verifyToken
    ],
    usersCtrl.getUsers
)
router.get(
    "/:id",
    [
        authJwt.verifyToken
    ],
    usersCtrl.getUser
)
router.post(
    "/me",
    [
        authJwt.verifyToken
    ],
    usersCtrl.getMe
)
router.put(
    "/change-password",
    [
        authJwt.verifyToken,
        //userSchema.createUser,
        //validator.validateRequestSchema,
    ],
    usersCtrl.changePassword
)
router.put(
    "/update-user",
    [
        authJwt.verifyToken,
        authJwt.isAdmin,
        userSchema.updateUser,
        //validator.validateRequestSchema,
    ],
    usersCtrl.updateUser
)
router.put(
    "/update-user/:id",
    [
        authJwt.verifyToken,
        authJwt.isAdmin,
        //userSchema.createUser,
        //validator.validateRequestSchema,
    ],
    usersCtrl.updateUserById
)
router.delete(
    "/:id",
    [
        authJwt.verifyToken,
        authJwt.isAdmin,
    ],
    usersCtrl.deleteUser
)
export default router