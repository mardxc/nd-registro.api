import { body } from 'express-validator'

export const create = [
    body('title')
        .isString()
        .withMessage('Title must contain a valid text'),
    body('description')
        .isString()
        .withMessage('Description must contain a valid text'),
    body('status')
        .isString()
        .withMessage('Status must contain a valid Status Id'),
    body('users')
        .isArray({ min: 1 })
        .withMessage('Status must contain a valid Array of User Id')

]