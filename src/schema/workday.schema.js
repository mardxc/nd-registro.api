import { body } from 'express-validator';

export const checkIn = [
    body('activity')
        .isString()
        .withMessage('Type must contain a valid activity id'),
    body('lat')
        .isDecimal()
        .withMessage('Lat must contain a valid text latitude'),
    body('lon')
        .isDecimal()
        .withMessage('Lon must contain a valid text longitude'),
];


export const getChecksByIdDate = [
    body('userId')
        .isString()
        .withMessage('Name must contain a valid user id'),
    body('date')
        .isDate()
        .withMessage('Date must contain a valid date'),
];