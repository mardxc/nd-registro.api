import { body } from 'express-validator'

export const create = [
    body('description')
        .isString()
        .withMessage('Description must contain a valid text')
]