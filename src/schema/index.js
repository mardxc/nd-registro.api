import * as authSchema from "./auth.schema";
import * as geolocationSchema from "./geolocation.schema";
import * as taskSchema from "./task.schema";
import * as userSchema from "./user.schema";
import * as workdaySchema from "./workday.schema";
import * as phaseSchema from "./phase.schema";
import * as statusSchema from "./status.schema";
import * as projectSchema from "./project.schema";
import * as activitySchema from "./activity.schema";
import * as delaySchema from "./delay.schema";
import * as criteriaSchema from "./criteria.schema";

export {
  delaySchema,
  criteriaSchema,
  activitySchema,
  projectSchema,
  statusSchema,
  phaseSchema,
  workdaySchema,
  userSchema,
  taskSchema,
  geolocationSchema,
  authSchema,
};
