import { body } from 'express-validator';

export const loginSchema = [
    body('email')
        .isEmail()
        .withMessage('email must contain a valid email address'),
];

export const registerSchema = [
    body('username')
        .isString()
        .withMessage('Username must contain a valid text'),
    body('email')
        .isEmail()
        .withMessage('Email must contain a valid email address'),
    body('password')
        .isString()
        .withMessage('Password must be at least 5 characters long'),
    body('name')
        .isString()
        .withMessage('Name  must contain a valid text'),
    body('lastname')
        .isString()
        .withMessage('Lastname  must contain a valid text'),
];

