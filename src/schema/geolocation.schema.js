import { body } from 'express-validator';

export const createGeolocation = [
    body('name')
        .isString()
        .withMessage('Name must contain a valid text'),
    body('lat')
        .isDecimal()
        .withMessage('lat must contain a valid text latitude'),
    body('lon')
        .isDecimal()
        .withMessage('lon must contain a valid text longitude'),
];

export const assingLocationUser = [
    body('userId')
        .isString()
        .withMessage('Name must contain a valid user id')
];