import { body } from 'express-validator';

export const createTask = [
    body('description')
        .isString()
        .withMessage('Description must contain a valid text'),
    body('status')
        .isString()
        .withMessage('Status must contain a valid status id'),
    body('activity')
        .isString()
        .withMessage('Status must contain a valid activity id'),
    body('projectId')
        .isString()
        .withMessage('Status must contain a valid project id'),
    body('phase')
        .isString()
        .withMessage('Status must contain a valid phase id'),
    body('hours')
        .isInt()
        .withMessage('Hours must contain a valid int'),
    body('userId')
        .isString()
        .withMessage('User Id must contain a valid user id'),
];

export const getTasksByUserDate = [
    body('userId')
        .isString()
        .withMessage('Name must contain a valid user id'),
    body('date')
        .isDate()
        .withMessage('Date must contain a valid date'),
];